This is the Debian GNU/Linux r-cran-tcltk2 package of tcl2tk.  The
tcltk2 package provides GNU R with Tcl/Tk extensions based on
Tcl/Tk2. The Tcl/Tk2 extension were written by a number of authors
(see file LICENSE in the sources and below); the R package was written
by Philippe Grosjean.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'tcltk2' to
'r-cran-tcltk2' to fit the pattern of CRAN (and non-CRAN) packages 
for R.

Files: R/*
Copyright: 2009 - 2013  Philippe Grosjean
License: LGPL-3

Files: debian/*
Copyright: 2013  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2

On a Debian GNU/Linux system, the LPGL license (version 3) is included
in the file /usr/share/common-licenses/LGPL-3.

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2.

The upstream LICENSE file:
-----------------------------------------------------------------------------
There are different licenses for different parts of this package:

== For the R code and most Tcl code license is:

GNU Lesser General Public License Version 3


== For the combobox2.3 and mclistbox1.2 Tk Widgets:

Combobox version 2.3
Copyright (c) 1998-2003, Bryan Douglas Oakley
All Rights Reserved.

Mclistbox version 1.2
Copyright (c) 1999, Bryan Douglas Oakley
All Rights Reserved.

This software is provided AS-IS with no waranty expressed or
implied. This software may be used free of charge, though I would
appreciate it if you give credit where credit is due and mention my
name when you use it.


== For the bwidget1.7 Tk Widgets, Tcllib 0.5 and Tklib 0.5:

The following terms apply to all files associated with the software
unless explicitly disclaimed in individual files.

The authors hereby grant permission to use, copy, modify, distribute,
and license this software and its documentation for any purpose, provided
that existing copyright notices are retained in all copies and that this
notice is included verbatim in any distributions. No written agreement,
license, or royalty fee is required for any of the authorized uses.
Modifications to this software may be copyrighted by their authors
and need not follow the licensing terms described here, provided that
the new terms are clearly indicated on the first page of each file where
they apply.

IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
MODIFICATIONS.

GOVERNMENT USE: If you are acquiring this software on behalf of the
U.S. government, the Government shall have only "Restricted Rights"
in the software and related documentation as defined in the Federal
Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2).  If you
are acquiring the software on behalf of the Department of Defense, the
software shall be classified as "Commercial Computer Software" and the
Government shall have only "Restricted Rights" as defined in Clause
252.227-7013 (c) (1) of DFARs.  Notwithstanding the foregoing, the
authors grant the U.S. Government and others acting in its behalf
permission to use and distribute the software in accordance with the
terms specified in this license.


== For ctext 3.2:

This software is copyrighted by George Peter Staplin.  The following 
terms apply to all files associated with the software unless explicitly
disclaimed in individual files.

The authors hereby grant permission to use, copy, modify, distribute,
and license this software and its documentation for any purpose, provided
that existing copyright notices are retained in all copies and that this
notice is included verbatim in any distributions. No written agreement,
license, or royalty fee is required for any of the authorized uses.
Modifications to this software may be copyrighted by their authors
and need not follow the licensing terms described here, provided that
the new terms are clearly indicated on the first page of each file where
they apply.

IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
MODIFICATIONS.


== For tree1.7:

I am D. Richard Hipp, the author of this code.  I hereby
disavow all claims to copyright on this program and release
it into the public domain.


== For tablelist5.5:

Multi-column listbox and tree widget package Tablelist, version 5.5
Copyright (c) 2000-2011  Csaba Nemethi (E-mail: csaba.nemethi@t-online.de)

This library is free software; you can use, modify, and redistribute it
for any purpose, provided that existing copyright notices are retained
in all copies and that this notice is included verbatim in any
distributions.

This software is distributed WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

-----------------------------------------------------------------------------


For reference, the upstream DESCRIPTION file is included below:

  Package: tcltk2
  Type: Package
  Version: 1.2-8
  Date: 2013-12-05
  Title: Tcl/Tk Additions
  Authors@R: c(person("Philippe", "Grosjean", role = c("aut", "cre"),
    email = "phgrosjean@sciviews.org"))
  Author: Philippe Grosjean
  Maintainer: Philippe Grosjean <phgrosjean@sciviews.org>
  Depends: R (>= 2.8.0), tcltk
  Suggests: utils
  SystemRequirements: Tcl/Tk (>= 8.5), Tktable (>= 2.9, optional)
  Description: A series of additional Tcl commands and Tk widgets with style
    and various functions (under Windows: DDE exchange, access to the
    registry and icon manipulation) to supplement the tcltk package
  License: LGPL-3 + file LICENSE
  URL: http://www.sciviews.org/SciViews-R
  BugReports: https://r-forge.r-project.org/tracker/?group_id=194
  Packaged: 2013-12-05 14:53:30 UTC; phgrosjean
  NeedsCompilation: no
  Repository: CRAN
  Date/Publication: 2013-12-05 19:22:00
  
